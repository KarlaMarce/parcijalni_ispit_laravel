<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Izmjeni o meni</h2>
        </div>
        <form method="POST" action="/about/{{$abouts->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naslov</label>
              <input type="text" name="headline" value="{{$abouts->headline}}" class="form-control" id="exampleInputHeadline" aria-describedby="headlineHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Sadrzaj</label>
                <input type="text" name="body" value="{{$abouts->body}}" class="form-control" id="exampleInputBody" aria-describedby="body">
            </div>
           
            <button type="submit" class="btn btn-primary">Spremi promjene</button>
        </form>
    </div>
</x-layout>