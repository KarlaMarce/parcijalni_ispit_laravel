<x-layout>
<div class="row">
    <div class="col-sm-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Ime i prezime</th>
                <th scope="col">O meni</th>
                @auth
                    <th scope="col">Akcije</th>
                @endauth
            </tr>
            </thead>
            <tbody>
                @foreach ($abouts as $key => $about)

                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$about->headline}}</td>
                    <td>{{$about->body}}</td>
                    @auth
                    <td>
                        <a class="btn btn-secondary"  href="/update_about/{{$about->id}}">Uredi post</a>
                         
                    </td>
                    @endauth
                </tr>
                    
                @endforeach
            
            </tbody>
        </table>

     </div>
    <div class="row">
        <div class="col-sm-12">
        </div>
</div>
 
</x-layout>