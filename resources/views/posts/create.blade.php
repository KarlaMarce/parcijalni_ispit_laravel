<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Unesi novi post</h2>
        </div>
        <form method="POST" action="/posts">
            @csrf
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naslov</label>
              <input type="text" name="headline" class="form-control" id="exampleInputHeadline" aria-describedby="headlineHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Sadrzaj</label>
                <input type="text" name="body" class="form-control" id="exampleInputBody" aria-describedby="body">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Datum kreiranja</label>
                <input type="text" name="date_created" class="form-control" id="exampleInputBody" aria-describedby="date_created">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</x-layout>