<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Izmjeni post</h2>
        </div>
        <form method="POST" action="/posts/{{$post->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naslov</label>
              <input type="text" name="headline" value="{{$post->headline}}" class="form-control" id="exampleInputHeadline" aria-describedby="headlineHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Sadrzaj</label>
                <input type="text" name="body" value="{{$post->body}}" class="form-control" id="exampleInputBody" aria-describedby="body">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Datum kreiranja</label>
                <input type="text" name="date_created" value="{{$post->date_created}}" class="form-control" id="exampleInputBody" aria-describedby="date_created">
            </div>
            <button type="submit" class="btn btn-primary">Spremi promjene</button>
        </form>
    </div>
</x-layout>