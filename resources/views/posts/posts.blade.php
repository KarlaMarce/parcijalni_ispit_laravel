<x-layout>
    @auth
    <div class="row">
        <div class="col-sm-2">
            <a class="btn btn-primary"  href="/create">Dodaj novi post</a>
        </div>
    </div>
    @endauth
<div class="row">
    <div class="col-sm-12">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Naslov posta</th>
                <th scope="col">Tekst</th>
                <th scope="col">Datum kreiranja</th>
                @auth
                    <th scope="col">Akcije</th>
                @endauth
            </tr>
            </thead>
            <tbody>
                @foreach ($posts as $key => $post)

                <tr>
                    <th scope="row">{{$key+1}}</th>
                    <td>{{$post->headline}}</td>
                    <td>{{$post->body}}</td>
                    <td>{{$post->date_created}}</td>
                    @auth
                    <td>
                        <a class="btn btn-secondary"  href="/update/{{$post->id}}">Uredi post</a>
                        <form method="POST" action="/posts/{{$post->id}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger"  type="submit">Izbrisi post</a>
                        </form>
                        
                    </td>
                    @endauth
                </tr>
                    
                @endforeach
            
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-sm-12">
        </div>
</div>
</x-layout>