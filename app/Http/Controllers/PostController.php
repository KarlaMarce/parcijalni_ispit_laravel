<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Exception;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index()
    {
        return view('posts.posts',
        [
            'posts' => Post::all()
        ]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $formFields = $request->validate([
            'headline' => 'required',
            'body' => 'required',
            'date_created' => 'required'
        ]
        );

        

        $formFields['user_id'] = auth()->id();

        //dd($formFields);

        Post::create($formFields);

        return redirect('/');
    }

    public function update($id)
    {
        $post = Post::find($id);
        return view('posts.update',[
            'post' => $post
        ]);
    }

    public function edit(Request $request, $id)
    {
        try{
            $post = Post::find($id);

            $post['headline']= $request->headline;
            $post['body']= $request->body;
            $post['date_created']= $request->date_created;
    
            if($post['user_id'] != auth()->id())
            {
                throw new Exception('Mijenjati moze samo korisnik koji je kreirao post');
            }
            else
            {
                $post->save();
            }
        }
        catch(Exception $e){
            echo 'Caught exception ', $e->getMessage(), "\n";
            return view('posts.error');

        }
        
        
        return redirect('/');
    }

    public function destroy($id)
    {
        $post= Post::find($id);

        $post->delete();

        return redirect('/');

    }
}
