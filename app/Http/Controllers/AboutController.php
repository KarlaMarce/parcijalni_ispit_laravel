<?php

namespace App\Http\Controllers;

use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    public function about_me()
    {
        return view('abouts.about',
        [
            'abouts' => About::all()
        ]);
    }

    public function update($id)
    {
        $about = About::find($id);
        return view('abouts.update',[
            'abouts' => $about
        ]);
    }

    public function edit(Request $request, $id)
    {
        $about = About::find($id);

        $about['headline']= $request->headline;
        $about['body']= $request->body;
        

        $about->save();
        return redirect('/about_me');
    }
}
