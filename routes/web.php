<?php

use App\Http\Controllers\AboutController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);
Route::get('/create', [PostController::class, 'create'])->middleware('auth');
Route::post('/posts', [PostController::class, 'store'])->middleware('auth');
Route::get('/update/{id}', [PostController::class, 'update'])->middleware('auth');
Route::put('/posts/{id}', [PostController::class, 'edit'])->middleware('auth');
Route::delete('/posts/{id}', [PostController::class, 'destroy'])->middleware('auth');

Route::get('/update_about/{id}', [AboutController::class, 'update'])->middleware('auth');
Route::put('/about/{id}', [AboutController::class, 'edit'])->middleware('auth');

Route::get('/about_me', [AboutController::class, 'about_me']);

Route::get('/registration', [UserController::class, 'registration']);
Route::get('/login', [UserController::class, 'login']);
Route::post('/user_registration', [UserController::class, 'registerUser']);
Route::post('/authentication', [UserController::class, 'authentication']);
Route::post('/logout', [UserController::class, 'logout'])->middleware('auth');
